package edu.hneu.mjt.Nazarenko.ticket_1_25.service;

import edu.hneu.mjt.Nazarenko.ticket_1_25.dto.AlbumDTO;
import edu.hneu.mjt.Nazarenko.ticket_1_25.entity.Album;
import edu.hneu.mjt.Nazarenko.ticket_1_25.mapper.AlbumMapper;
import edu.hneu.mjt.Nazarenko.ticket_1_25.repository.AlbumRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AlbumService {

    private final AlbumRepository albumRepository;

    public AlbumService(AlbumRepository albumRepository) {
        this.albumRepository = albumRepository;
    }

    public AlbumDTO createAlbum(AlbumDTO albumDTO) {
        Album album = AlbumMapper.INSTANCE.albumDtoToAlbum(albumDTO);
        album = albumRepository.save(album);
        return AlbumMapper.INSTANCE.albumToAlbumDto(album);
    }

    public List<AlbumDTO> getAllAlbums() {
        return albumRepository.findAll()
                .stream()
                .map(AlbumMapper.INSTANCE::albumToAlbumDto)
                .collect(Collectors.toList());
    }

    public AlbumDTO getAlbumById(Long id) {
        Album album = albumRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Album with id " + id + " not found"));
        return AlbumMapper.INSTANCE.albumToAlbumDto(album);
    }

    public AlbumDTO updateAlbum(Long id, AlbumDTO albumDTO) {
        Album album = albumRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Album with id " + id + " not found"));
        album.setArtist(albumDTO.getArtist());
        album.setGenre(albumDTO.getGenre());
        album.setReleaseDate(albumDTO.getReleaseDate());
        album = albumRepository.save(album);
        return AlbumMapper.INSTANCE.albumToAlbumDto(album);
    }

    public void deleteAlbum(Long id) {
        albumRepository.deleteById(id);
    }
}
