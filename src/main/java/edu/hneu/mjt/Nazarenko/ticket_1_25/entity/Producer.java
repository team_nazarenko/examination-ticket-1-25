package edu.hneu.mjt.Nazarenko.ticket_1_25.entity;

import lombok.Data;
import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;
@Entity
@Data
public class Producer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private Integer experience;

    @Column(nullable = false)
    private String genre;

    @Column(nullable = false)
    private String phoneNumber;

    @OneToMany(mappedBy = "producer", cascade = CascadeType.ALL)
    private List<Album> albums = new ArrayList<>();
}
