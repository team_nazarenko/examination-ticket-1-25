package edu.hneu.mjt.Nazarenko.ticket_1_25.mapper;

import edu.hneu.mjt.Nazarenko.ticket_1_25.dto.AlbumDTO;
import edu.hneu.mjt.Nazarenko.ticket_1_25.entity.Album;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface AlbumMapper {

    AlbumMapper INSTANCE = Mappers.getMapper(AlbumMapper.class);

    AlbumDTO albumToAlbumDto(Album album);

    Album albumDtoToAlbum(AlbumDTO albumDTO);
}
