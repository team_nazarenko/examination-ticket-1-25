package edu.hneu.mjt.Nazarenko.ticket_1_25.controller;

import edu.hneu.mjt.Nazarenko.ticket_1_25.dto.AlbumDTO;
import edu.hneu.mjt.Nazarenko.ticket_1_25.service.AlbumService;
import edu.hneu.mjt.Nazarenko.ticket_1_25.service.ProducerService;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/albums")
public class AlbumRestController {

    private final AlbumService albumService;
    private final ProducerService producerService;

    public AlbumRestController(AlbumService albumService, ProducerService producerService) {
        this.albumService = albumService;
        this.producerService = producerService;
    }

    @GetMapping
    public List<AlbumDTO> listAlbums() {
        return albumService.getAllAlbums();
    }

    @PostMapping
    public ResponseEntity<AlbumDTO> createAlbum(@Valid @RequestBody AlbumDTO albumDTO) {
        AlbumDTO createdAlbum = albumService.createAlbum(albumDTO);
        return new ResponseEntity<>(createdAlbum, HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<AlbumDTO> getAlbumById(@PathVariable Long id) {
        AlbumDTO albumDTO = albumService.getAlbumById(id);
        return new ResponseEntity<>(albumDTO, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<AlbumDTO> updateAlbum(@PathVariable Long id, @Valid @RequestBody AlbumDTO albumDTO) {
        AlbumDTO updatedAlbum = albumService.updateAlbum(id, albumDTO);
        return new ResponseEntity<>(updatedAlbum, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteAlbum(@PathVariable Long id) {
        albumService.deleteAlbum(id);
        return ResponseEntity.noContent().build();
    }
}
