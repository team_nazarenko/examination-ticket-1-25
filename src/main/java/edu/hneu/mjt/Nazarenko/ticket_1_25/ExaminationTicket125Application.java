package edu.hneu.mjt.Nazarenko.ticket_1_25;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExaminationTicket125Application {

    public static void main(String[] args) {
        SpringApplication.run(ExaminationTicket125Application.class, args);
    }

}
