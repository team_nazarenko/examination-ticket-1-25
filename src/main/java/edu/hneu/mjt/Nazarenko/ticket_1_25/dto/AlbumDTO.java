package edu.hneu.mjt.Nazarenko.ticket_1_25.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.time.LocalDate;
@Data
public class AlbumDTO {
    private Long id;

    @NotBlank(message = "Artist name cannot be empty")
    private String artist;

    @NotBlank(message = "Genre cannot be empty")
    private String genre;
    private LocalDate releaseDate;

    @NotNull(message = "Producer cannot be empty")
    private ProducerDTO producer;
}
