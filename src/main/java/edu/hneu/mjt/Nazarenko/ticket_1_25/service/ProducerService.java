package edu.hneu.mjt.Nazarenko.ticket_1_25.service;

import edu.hneu.mjt.Nazarenko.ticket_1_25.dto.ProducerDTO;
import edu.hneu.mjt.Nazarenko.ticket_1_25.entity.Producer;
import edu.hneu.mjt.Nazarenko.ticket_1_25.mapper.ProducerMapper;
import edu.hneu.mjt.Nazarenko.ticket_1_25.repository.ProducerRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
@Service
public class ProducerService {

    private final ProducerRepository producerRepository;

    public ProducerService(ProducerRepository producerRepository) {
        this.producerRepository = producerRepository;
    }

    public ProducerDTO createProducer(ProducerDTO producerDTO) {
        Producer producer = ProducerMapper.INSTANCE.producerDtoToProducer(producerDTO);
        producer = producerRepository.save(producer);
        return ProducerMapper.INSTANCE.producerToProducerDto(producer);
    }

    public List<ProducerDTO> getAllProducers() {
        return producerRepository.findAll()
                .stream()
                .map(ProducerMapper.INSTANCE::producerToProducerDto)
                .collect(Collectors.toList());
    }

    public ProducerDTO getProducerById(Long id) {
        Producer producer = producerRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Producer with id " + id + " not found"));
        return ProducerMapper.INSTANCE.producerToProducerDto(producer);
    }

    public ProducerDTO updateProducer(Long id, ProducerDTO producerDTO) {
        Producer producer = producerRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Producer with id " + id + " not found"));
        producer.setName(producerDTO.getName());
        producer.setExperience(producerDTO.getExperience());
        producer.setGenre(producerDTO.getGenre());
        producer.setPhoneNumber(producerDTO.getPhoneNumber());
        producer = producerRepository.save(producer);
        return ProducerMapper.INSTANCE.producerToProducerDto(producer);
    }

    public void deleteProducer(Long id) {
        producerRepository.deleteById(id);
    }
}
