package edu.hneu.mjt.Nazarenko.ticket_1_25.mapper;

import edu.hneu.mjt.Nazarenko.ticket_1_25.dto.ProducerDTO;
import edu.hneu.mjt.Nazarenko.ticket_1_25.entity.Producer;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
@Mapper
public interface ProducerMapper {
    ProducerMapper INSTANCE = Mappers.getMapper(ProducerMapper.class);
    ProducerDTO producerToProducerDto(Producer producer);
    Producer producerDtoToProducer(ProducerDTO producerDTO);
}

