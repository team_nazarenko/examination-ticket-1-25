package edu.hneu.mjt.Nazarenko.ticket_1_25.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.Data;

@Data
public class ProducerDTO {
    private Long id;

    @NotBlank(message = "Producer name cannot be empty")
    @NotNull(message = "Field cannot be empty")
    private String name;

    @NotNull(message = "Field cannot be empty")
    private Integer experience;

    @NotBlank(message = "Genre cannot be empty")
    @NotNull(message = "Field cannot be empty")
    private String genre;

    @NotBlank(message = "Phone number cannot be empty")
    @NotNull(message = "Field cannot be empty")
    @Pattern(regexp = "^\\+380\\(\\d{2}\\)\\d{3}-\\d{2}-\\d{2}$", message = "Phone number must be in the format +380(XX)XXX-XX-XX")
    private String phoneNumber;
}
