package edu.hneu.mjt.Nazarenko.ticket_1_25.controller;

import edu.hneu.mjt.Nazarenko.ticket_1_25.dto.ProducerDTO;
import edu.hneu.mjt.Nazarenko.ticket_1_25.service.ProducerService;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/producers")
public class ProducerRestController {

    private final ProducerService producerService;

    public ProducerRestController(ProducerService producerService) {
        this.producerService = producerService;
    }

    @GetMapping
    public List<ProducerDTO> listProducers() {
        return producerService.getAllProducers();
    }

    @PostMapping
    public ResponseEntity<ProducerDTO> createProducer(@Valid @RequestBody ProducerDTO producerDTO) {
        ProducerDTO createdProducer = producerService.createProducer(producerDTO);
        return new ResponseEntity<>(createdProducer, HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProducerDTO> getProducerById(@PathVariable Long id) {
        ProducerDTO producerDTO = producerService.getProducerById(id);
        return new ResponseEntity<>(producerDTO, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ProducerDTO> updateProducer(@PathVariable Long id, @Valid @RequestBody ProducerDTO producerDTO) {
        ProducerDTO updatedProducer = producerService.updateProducer(id, producerDTO);
        return new ResponseEntity<>(updatedProducer, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteProducer(@PathVariable Long id) {
        producerService.deleteProducer(id);
        return ResponseEntity.noContent().build();
    }
}
