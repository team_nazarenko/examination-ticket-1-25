package edu.hneu.mjt.Nazarenko.ticket_1_25.repository;

import edu.hneu.mjt.Nazarenko.ticket_1_25.entity.Producer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProducerRepository extends JpaRepository<Producer, Long>{
}
